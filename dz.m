function vpd = dz(t,y)
%VDP1  Evaluate the van der Pol ODEs for mu = 1
%
%   See also ODE113, ODE23, ODE45.

%   Jacek Kierzenka and Lawrence F. Shampine
%   Copyright 1984-2014 The MathWorks, Inc.

vpd = [y(2)-2; (1-y(1)^2)*y(2)-y(1)];  %Merge to main

% y1' = y2;
% y2' = miu(y-y1^2)y2-y1
end